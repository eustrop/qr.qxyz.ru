#!/bin/sh
#
# LICENSE=BALES or MIT, uses optional LGPL componetnt
#

VSERVER_PATH=`dirname ${DOCUMENT_ROOT}`
VSERVER_ETC=${VSERVER_PATH}/etc

#echo "Content-type: image/png"
echo "Content-type: text/plain"
echo "Allow: GET, HEAD, PUT"
echo


if [ "${DOCUMENT_ARGS}x" != "x" ]; then
 QUERY_STRING=${DOCUMENT_ARGS}
 export QUERY_STRING
fi
cat ${VSERVER_ETC}/templates/print/GoDEXG500-100x70.html | sed "s/SUBST_QR_CODE_QUERY/${QUERY_STRING}/g"
