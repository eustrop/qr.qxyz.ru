#!/bin/sh
#
# LICENSE=BALES or MIT, uses optional LGPL componetnt
#

VSERVER_PATH=`dirname ${DOCUMENT_ROOT}`
VSERVER_ETC=${VSERVER_PATH}/etc
IHTML_FILE="${DOCUMENT_ROOT}/header.ihtml"
QRENOCDECMD=/usr/local/bin/qrencode
GETPARAMCMD=${VSERVER_PATH}/bin/get_qrimaget_param.qwk

echo "Content-type: image/png"
#echo "Content-type: text/plain"
echo "Allow: GET, HEAD, PUT"
echo


qr_encode_stdout()
{
if [ ! -x $QRENOCDECMD ]; then
 echo  $QRENOCDECMD not found
 return 1
fi
if [ ! -x $GETPARAMCMD ]; then
 GETPARAMCMD="echo Error"
fi
#$QRENOCDECMD -t PNG -o - "http://qr.qxyz.ru?q=FFFFFFF"
$GETPARAMCMD | $QRENOCDECMD -s 5 -t PNG -o -
#$GETPARAMCMD
}

#qr_encode_stdout
#set
#$GETPARAMCMD
qr_encode_stdout 2> /dev/stdout
