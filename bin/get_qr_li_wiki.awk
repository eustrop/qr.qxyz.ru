#!/usr/bin/awk -f

BEGIN{
FS=";";
}

function print_item(f,v)
{
print ";;" f ";" v;
}

{
print "";
print_item("Модель",$2 $3);
print_item("SN",$4);
print_item("Произведено",$5);
print_item("Продано",$6);
print_item("Отгружено",$7);
print_item("Договор",$8 " от " $9);
print_item("Начало гарантии",$10);
print_item("Окончание гарантии",$11);
print_item("Комментарии",$12);
print "";
}

