#!/usr/bin/awk -f
# qr.qxyz.ru
# (c) EustroSoft.org 2019
# LICENSE: BALES or MIT on your choice
#
# url examples:
#  http://qr.qxyz.ru/?q=123
#	012/3
#  http://qr.qxyz.ru/?q=1234
#	123/4
#  http://qr.qxyz.ru/?q=12345
#	0123/45
#  http://qr.qxyz.ru/?q=123456
#	1234/56
#  http://qr.qxyz.ru/?q=1234567
#	01234/567
#  http://qr.qxyz.ru/?q=1234567&p=123&d=/l/models/TDME/490
#	01234/567 123 /l/models/TDME/490

BEGIN{
 QR_Q="0"; # q=0 (main request parameter)
 QR_P="none"; # p=0 (optional request protection parameter)
 QR_D=""; # d= (optional request document parameter)
 DOCUMENT_ARGS = ENVIRON["DOCUMENT_ARGS"];
 param_count = split(DOCUMENT_ARGS,parameters,"&");
 for(i=1;i<=param_count;i++)
 {
  ppart_count= split(parameters[i],pparts,"=");
  if(ppart_count != 2) continue;
  if(pparts[1]=="q") QR_Q = pparts[2];
  if(pparts[1]=="p") QR_P = pparts[2];
  if(pparts[1]=="d") QR_D = pparts[2];
 }
 QR_Q = fix_q(QR_Q);
 QR_P = fix_p(QR_P);
 QR_D = fix_d(QR_D);
 print QR_Q, QR_P, QR_D;
} #/BEGIN
# fix & normalize passed parameter
function fix_q(v, q_number, hex, len_hex )
{
 if(v == "") return("error/nostring");
 q_number = hex2num(v);
 if(q_number == "" ) return("error/nohex");
 if(q_number < 0 ) return("error/negative");
 if(q_number > 2147483647 ) return("error/tolarge");
 if(q_number >=  2^24) hex = sprintf("%08X",q_number);
 if(q_number < 2^16) hex = sprintf("%04X",q_number);
 else  hex = sprintf("%06X",q_number);
 if(hex=="") return("error/internal_range");
 len_hex=length(hex);
 if(len_hex == 7){len_hex=8; hex= "0" hex; }
 v = "error/internal_substr";
 if(len_hex == 4){ v = substr(hex,1,3) "/" substr(hex,4,1); }
 if(len_hex == 6){ v = substr(hex,1,4) "/" substr(hex,5,2); }
 if(len_hex == 8){ v = substr(hex,1,5) "/" substr(hex,6,3); }
 return v;
}
function fix_p(v,  symbols)
{
 if(v == "") return("none");
 return v;
}
function fix_d(v) { return v; }
function hex2num(hex, hex_up, digits, digit, len,i, result)
{
 hex = toupper(hex);
 len=length(hex);
 if(len>8) return("");
 split(hex,digits,"");
 result=0;
 for(i=1;i<=len;i++)
 {
  digit="";
  if(digits[i] == "0" ) digit = 0;
  if(digits[i] == "1" ) digit = 1
  if(digits[i] == "2" ) digit = 2;
  if(digits[i] == "3" ) digit = 3;
  if(digits[i] == "4" ) digit = 4;
  if(digits[i] == "5" ) digit = 5;
  if(digits[i] == "6" ) digit = 6;
  if(digits[i] == "7" ) digit = 7;
  if(digits[i] == "8" ) digit = 8;
  if(digits[i] == "9" ) digit = 9;
  if(digits[i] == "A" ) digit = 10;
  if(digits[i] == "B" ) digit = 11;
  if(digits[i] == "C" ) digit = 12;
  if(digits[i] == "D" ) digit = 13;
  if(digits[i] == "E" ) digit = 14;
  if(digits[i] == "F" ) digit = 15;
  if(digit == "") return("");
  result=result*16 + digit;
 }
 return(result);
}
END{ }
