#;;;;;;;;;;;;;;;;
#;;Specification of diesel engine/Спецификация дизельного двигателя;;;;;;;;;;;;;;
#;EN;RU;CH;MU;MU_GLE;;;;;;;;;;;
#MODELS;Model;Model/Модель;;;;QC380;QC385;QC480;QC485;QC490;QC4100;QC4102;QC4105;QC4108;QC4112;QC6112
#1MODELS
#2Model
#3Model/Модель
#4
#5
#6
#7QC380
#8QC385
== Карточка модели: QC385
;;=#ID;=#Parameter;=Параметр;=#CH;=[MU];=eq;=Значение;=#Value
;;1;Type;Тип;;;;Вертикальный, водяного охлаждения, четырехтактный;Vertical, water-cooling, four-stroke
;;2;Combustion chamber type;Тип впрыска топлива;;;;Прямой впрыск;Direct injection
;;3;Number of cylinders;Число цилиндров;;;;3;3
;;4;Bore;Диаметр цилиндра;;мм;;80;80
;;5;Stroke;Ход поршня;;мм;;90;90
;;6;Compression ratio;Степень сжатия;;;;18:1;18:1
;;7;Total diplacement;Объем двигателя;;литры;;1.532;1.532
;;8;Firing order;Порядок работы цилиндров;;;;1-3-2;1-3-2
;;9;Rated power;Номинальная мощность;;кВт;;23.9;23.9
;;10;Speed;Скорость вращения;;об/мин;;3000;3000
;;11;Maximum torque/Максимальный крутящий момент;Максимальный крутящий момент;;Нм;;83.7;83.7
;;12;Maximum torque speed;Скорость при максимальном крутящем моменте;;об/мин;;2100;2100
;;13;Maximum speed;Максимальная скорость вращения;;об/мин;;3300;3300
;;14;Min. specifie consuption at full load;Минимальный расход топлива;;г/кВт*ч;<=;248;248
;;15;Specific lube oil consumption;Минимальный расход масла;;г/кВт*ч;<=;1.63;1.63
;;16;Piston mean speed;Средняя скорость поршня;;м/с;;9;9
;;17;Mean effective Pressure;Среднее эффективное давление;;кПа;;590;590
;;18;Direction of crankshaft rotation;Направление вращения двигателя;;;;Против часовой стрелки со стороны маховика;Counterclockwise facing to flywheel
;;19;Intake type;Наддув;;;;Атмосферный;Aspirate
;;20;Cooling method;Тип охлаждения;;;;Принудительное водяное охлаждение;Forced water Cooling
;;21;Lubricating method;Тип смазки;;;;Сочетание смазки под давлением и разбрызгиванием;Combination of pressure and splash
;;22;Starting method;Тип запуска;;;;Электростартер;Electric
;;23;Flyweel size;Размер маховика;;;;SAE 7.5;SAE 7.5
;;24;Bell housing size;Размер картера маховика;;;;SAE 4;SAE 4
;;25;Net mass;Вес;;кг;;175;175
;;26;Overall dimention;Габаритные размеры;;мм;;598×560×630;598×560×630
#;;Specification of main accessories/Спецификация основных агрегатов;;;;;;;;;;;;;;
#PARTS;;;;;;;;;;;;;;;;
== Спецификация основных агрегатов
#==EN Specification of main accessories
;;=#ID;=#Parameter;=Параметр;=#CH;=[MU];=eq;=Значение;=#Value
;;1;Fuel injection pump typel;Тип топливного насоса;;;;В линию, плунжерный;In-line, Plunger type
;;2;Fuel injection pump model;Модель топливного насоса;;;;3IW211-70BHF4IW070020;3IW211-70BHF4IW070020
;;3;Fuel injection pump governor;Регулятор топливного насоса;;;;Всережимный, механический;TQB All-speed mechanical centrifugal
;;4;Fuel delivery pump;Топливоподкачивающий насос;;;;Одностороннего действия, поршневой;Single-acting piston type
;;5;Fuel injector model;Модель форсунки;;;;PF68SD;PF68SD
;;6;Fuel injector diameter of nozzle hole;Диаметр отверстия распылителя;;ммм;;0.25;0.25
;;7;Fuel injector pressure;Давление впрыска;;Мпа;;20;20
;;8;Lube pump type;Тип масляного насоса;;;;Роторного типа;Rotor
;;9;Lube pump speed;Скорость вращения масляного насоса;;об/мин;;1300;1300
;;10;Lube pump capacity;Производительность масляного насоса;;литры/мин;;15;15
;;11;Lube pump pressure;Давление масляного насоса;;кПа;;400;400
;;12;Water pump model;Модель помпы;;;;Центробежный, односекционный;Centrifugal, volute, single-suction
;;13;Water pump speed;Скорость вращения помпы;;об/мин;;3000;3000
;;14;Water pump capacity;Производительность помпы;;литры/мин;;80;80
;;15;Starting motor model;Модель стартера;;;;QDJ132A;QDJ132A
;;16;Starting motor power;Мощность стартера;;кВт;;2.5;2.5
;;17;Alternator model;Модель генератора;;;;JFWBZ15;JFWBZ15
;;18;Alternator voltage;Напряжение генератора;;В;;14;14
;;19;Alternator power;Мощность генератора;;Вт;;500;500
;;20;Fuel filter;Топливный фильтр;;;;CX0706;CX0706
;;21;Lube oil filter;Масляный фильтр;;;;JX0706P1;JX0706P1
;;22;Air filter;Воздушный фильтр;;;;K1317;K1317
;;23;Belt width;Ширина ремня генератора;;мм;;13;13
;;24;Belt leight;Длина ремня генератора;;мм;;965;965
#;;Main technical data of diesel engine/Основные технические характеристики дизельного двигателя;;;;;;;;;;;;;;
#TECHDATA{;;;;;;;;;;;;;;;;
== Основные технические характеристики дизельного двигателя
#==EN Main technical data of diesel engine
;;=#ID;=#Parameter;=Параметр;=#CH;=[MU];=eq;=Значение;=#Value
;;1;Valve lash of intake valve;Зазор впускного клапана;;мм;;0.25;0.25
;;2;Valve lash of exhaust valve;Зазор выпускного клапана;;мм;;0.30;0.30
;;3;Sinkage of valve;Просадка клапана;;мм;;0.9;0.9
;;4;Exhaust temperature;Температура выхлопных газов;;°C;;550;550
;;5;Cooling water temperature;Температура охлаждающей жидкости;;°C;;95;95
;;6;Lube oil temperature;Температура масла;;°C;;105;105
;;7;Lube oil pressure at normal operation;Давление масла рабочее;;Мпа;;0.5;0.5
;;8;Lube oil pressure at min. steady speed;Давление масла при минимальных оборотах;;МПа;;0.08;0.08
;;9;Oil sump capacity;Объем масла;;литры;;4.5;4.5
;;10;Advance Injection Angle;Угол опережения впрыска;;°;;16;16
;;11;Min. idling steady speed;Холостые обороты;;об/мин;>;850;850
;;12;Steady regulation;Колебания оборотов;;%;;5;5
;;13;Valve timing inlet open;Угол открытия впускного клапана;;°;;;
;;14;Valve timing inlet close;Угол закрытия впускного клапана;;°;;;
;;15;Valve timing outlet open;Угол открытия выпускного клапана;;°;;;
;;16;Valve timing outlet close;Угол закрытия выпускного клапана;;°;;;
#;;Torque limits of main bolts and nuts/Моменты затяжки основных болтов и гаек;;;;;;;;;;;;;;
#TORQUE{;;;;;;;;;;;;;;;;
== Моменты затяжки основных болтов и гаек
#==EN Torque limits of main bolts and nuts
;;=#ID;=#Parameter;=Параметр;=#CH;=[MU];=eq;=Значение;=#Value
;;1;Cylinder head bolts;Болты головки блока;;Нм;;160;160
;;2;Main bearing cap bolts;Болты крышки коренных подшипников;;Нм;;125;125
;;3;Connecting rod bolts;Болты крышек шатунов;;Нм;;55;55
;;4;Flywheel bolts;Болты маховика;;Нм;;65;65
;;5;Crankshaft pulley blots;Болт шкива коленчатого вала;;Нм;;150;150
#;;Clearance of main parts /Установочные зазоры основных деталей;;;;;;;;;;;;;;
#CLEARANCE{;;;;;;;;;;;;;;;;
== Установочные зазоры основных деталей
#==EN Clearance of main parts 
;;=#ID;=#Parameter;=Параметр;=#CH;=[MU];=eq;=Значение;=#Value
;;1;Connecting rod journal and connecting rod bushing;Шатунная шейка коленчатого вала и втулка малой головки шатуна;;мм;;0.040-0.089;0.040-0.089
;;2;Piston pin and connecting;Поршневой палец;;мм;;0.025-0.046;0.025-0.046
;;3;Piston skirt and cylinder liner;Юбка поршня и гильза;;мм;;0.106-0.160;0.106-0.160
;;4;Side clearance between the 1st ring and its groove;Зазор между первым компрессионным кольцом и канавкой;;мм;;0.060-0.092;0.060-0.092
;;5;Side clearance between the 2nd ring its groove;Зазор между вторым компрессионным кольцом и канавкой;;мм;;0.040-0.072;0.040-0.072
;;6;Side clearance between the oil scraper ring and its groove;Зазор между маслосъемным кольцом и канавкой;;мм;;0.030-0.067;0.030-0.067
;;7;Gap of the 1st ring;Зазор замка первого компрессионного кольца;;мм;;0.250-0.400;0.250-0.400
;;8;Gap of the 2nd ring and oil scraper ring;Зазор замка второго компрессионного и маслосъемного колец;;мм;;0.250-0.400;0.250-0.400
;;9;Crankshaft main journal and main bearing;Шейка коленчатого вала;;мм;;0.070-0.139;0.070-0.139
;;10;Camshaft journal and bushing;Шейка распределительного вала;;мм;;0.050-0.100;0.050-0.100
;;11;Idle gear shaft journal and bushing;Шейка промежуточного вала;;мм;;0.025-0.075;0.025-0.075
;;12;Intake valve stem and valve guide;Направляющая впускного клапана;;мм;;0.025-0.069;0.025-0.069
;;13;Exhaust valve stem and valve guide;Направляющая выпускного клапана;;мм;;0.040-0.077;0.040-0.077
;;14;Rocker arm shaft and bushing;Шейка вала толкателей;;мм;;0.016-0.061;0.016-0.061
;;15;Axial clearance of crankshaft;Осевой зазор коленчатого вала;;мм;;0.075-0.265;0.075-0.265
;;16;Axial clearance of camshaft;Осевой зазор распределительного вала;;мм;;0.050-0.220;0.050-0.220
#;;Wear limit of main parts /Лимит износа основных деталей;;;;;;;;;;;;;;
#WEAR{;;;;;;;;;;;;;;;;
== Лимит износа основных деталей
#==EN Wear limit of main parts 
;;=#ID;=#Parameter;=Параметр;=#CH;=[MU];=eq;=Значение;=#Value
;;1;Connecting rod journal and connecting rod bushing;Шатунная шейка коленчатого вала и втулка малой головки шатуна;;мм;;0.20;0.20
;;2;Piston pin and connecting;Поршневой палец;;мм;;0.10;0.10
;;3;Piston skirt and cylinder liner;Юбка поршня и гильза;;мм;;0.40;0.40
;;4;Side clearance between the 1st ring and its groove;Зазор между первым компрессионным кольцом и канавкой;;мм;;0.20;0.20
;;5;Side clearance between the 2nd ring its groove;Зазор между вторым компрессионным кольцом и канавкой;;мм;;0.18;0.18
;;6;Side clearance between the oil scraper ring and its groove;Зазор между маслосъемным кольцом и канавкой;;мм;;0.18;0.18
;;7;Gap of the 1st ring;Зазор замка первого компрессионного кольца;;мм;;1.60;1.60
;;8;Gap of the 2nd ring and oil scraper ring;Зазор замка второго компрессионного и маслосъемного колец;;мм;;2.20;2.20
;;9;Crankshaft main journal and main bearing;Шейка коленчатого вала;;мм;;0.25;0.25
;;10;Camshaft journal and bushing;Шейка распределительного вала;;мм;;0.18;0.18
;;11;Idle gear shaft journal and bushing;Шейка промежуточного вала;;мм;;0.18;0.18
;;12;Intake valve stem and valve guide;Направляющая впускного клапана;;мм;;0.15;0.15
;;13;Exhaust valve stem and valve guide;Направляющая выпускного клапана;;мм;;0.15;0.15
;;14;Rocker arm shaft and bushing;Шейка вала толкателей;;мм;;0.20;0.20
;;15;Axial clearance of crankshaft;Осевой зазор коленчатого вала;;мм;;;
;;16;Axial clearance of camshaft;Осевой зазор распределительного вала;;мм;;;
