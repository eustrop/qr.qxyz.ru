#;;;;;;;;;;;;;;;;
#;;Specification of diesel engine/Спецификация дизельного двигателя;;;;;;;;;;;;;;
#;EN;RU;CH;MU;MU_GLE;;;;;;;;;;;
#MODELS;Model;Model/Модель;;;;QC380;QC385;QC480;QC485;QC490;QC4100;QC4102;QC4105;QC4108;QC4112;QC6112
#1MODELS
#2Model
#3Model/Модель
#4
#5
#6
#7QC380
#8QC385
#9QC480
#10QC485
#11QC490
#12QC4100
#13QC4102
#14QC4105
#15QC4108
#16QC4112
#17QC6112
== Карточка модели: QC6112
;;=#ID;=#Parameter;=Параметр;=#CH;=[MU];=eq;=Значение;=#Value
;;1;Type;Тип;;;;Вертикальный, водяного охлаждения, четырехтактный;Vertical, water-cooling, four-stroke
;;2;Combustion chamber type;Тип впрыска топлива;;;;Прямой впрыск;Direct injection
;;3;Number of cylinders;Число цилиндров;;;;6;6
;;4;Bore;Диаметр цилиндра;;мм;;112;112
;;5;Stroke;Ход поршня;;мм;;135;135
;;6;Compression ratio;Степень сжатия;;;;17:1;17:1
;;7;Total diplacement;Объем двигателя;;литры;;7.980;7.980
;;8;Firing order;Порядок работы цилиндров;;;;;
;;9;Rated power;Номинальная мощность;;кВт;;;
;;10;Speed;Скорость вращения;;об/мин;;2200;2200
;;11;Maximum torque/Максимальный крутящий момент;Максимальный крутящий момент;;Нм;;534;534
;;12;Maximum torque speed;Скорость при максимальном крутящем моменте;;об/мин;;;
;;13;Maximum speed;Максимальная скорость вращения;;об/мин;;;
;;14;Min. specifie consuption at full load;Минимальный расход топлива;;г/кВт*ч;<=;215;215
;;15;Specific lube oil consumption;Минимальный расход масла;;г/кВт*ч;<=;;
;;16;Piston mean speed;Средняя скорость поршня;;м/с;;;
;;17;Mean effective Pressure;Среднее эффективное давление;;кПа;;;
;;18;Direction of crankshaft rotation;Направление вращения двигателя;;;;Против часовой стрелки со стороны маховика;Counterclockwise facing to flywheel
;;19;Intake type;Наддув;;;;Турбонаддув;Turbocharge
;;20;Cooling method;Тип охлаждения;;;;Принудительное водяное охлаждение;Forced water Cooling
;;21;Lubricating method;Тип смазки;;;;Сочетание смазки под давлением и разбрызгиванием;Combination of pressure and splash
;;22;Starting method;Тип запуска;;;;Электростартер;Electric
;;23;Flyweel size;Размер маховика;;;;SAE 11.5;SAE 11.5
;;24;Bell housing size;Размер картера маховика;;;;SAE 2;SAE 2
;;25;Net mass;Вес;;кг;;;
;;26;Overall dimention;Габаритные размеры;;мм;;1200х723х1110;1200х723х1110
#;;Specification of main accessories/Спецификация основных агрегатов;;;;;;;;;;;;;;
#PARTS;;;;;;;;;;;;;;;;
== Спецификация основных агрегатов
#==EN Specification of main accessories
;;=#ID;=#Parameter;=Параметр;=#CH;=[MU];=eq;=Значение;=#Value
;;1;Fuel injection pump typel;Тип топливного насоса;;;;В линию, плунжерный;In-line, Plunger type
;;2;Fuel injection pump model;Модель топливного насоса;;;;;
;;3;Fuel injection pump governor;Регулятор топливного насоса;;;;Всережимный, механический;TQB All-speed mechanical centrifugal
;;4;Fuel delivery pump;Топливоподкачивающий насос;;;;Одностороннего действия, поршневой;Single-acting piston type
;;5;Fuel injector model;Модель форсунки;;;;;
;;6;Fuel injector diameter of nozzle hole;Диаметр отверстия распылителя;;ммм;;;
;;7;Fuel injector pressure;Давление впрыска;;Мпа;;;
;;8;Lube pump type;Тип масляного насоса;;;;Роторного типа;Rotor
;;9;Lube pump speed;Скорость вращения масляного насоса;;об/мин;;;
;;10;Lube pump capacity;Производительность масляного насоса;;литры/мин;;;
;;11;Lube pump pressure;Давление масляного насоса;;кПа;;;
;;12;Water pump model;Модель помпы;;;;Центробежный, односекционный;Centrifugal, volute, single-suction
;;13;Water pump speed;Скорость вращения помпы;;об/мин;;;
;;14;Water pump capacity;Производительность помпы;;литры/мин;;;
;;15;Starting motor model;Модель стартера;;;;;
;;16;Starting motor power;Мощность стартера;;кВт;;;
;;17;Alternator model;Модель генератора;;;;;
;;18;Alternator voltage;Напряжение генератора;;В;;28;28
;;19;Alternator power;Мощность генератора;;Вт;;;
;;20;Fuel filter;Топливный фильтр;;;;;
;;21;Lube oil filter;Масляный фильтр;;;;;
;;22;Air filter;Воздушный фильтр;;;;;
;;23;Belt width;Ширина ремня генератора;;мм;;22;22
;;24;Belt leight;Длина ремня генератора;;мм;;1448;1448
#;;Main technical data of diesel engine/Основные технические характеристики дизельного двигателя;;;;;;;;;;;;;;
#TECHDATA{;;;;;;;;;;;;;;;;
== Основные технические характеристики дизельного двигателя
#==EN Main technical data of diesel engine
;;=#ID;=#Parameter;=Параметр;=#CH;=[MU];=eq;=Значение;=#Value
;;1;Valve lash of intake valve;Зазор впускного клапана;;мм;;;
;;2;Valve lash of exhaust valve;Зазор выпускного клапана;;мм;;;
;;3;Sinkage of valve;Просадка клапана;;мм;;;
;;4;Exhaust temperature;Температура выхлопных газов;;°C;;;
;;5;Cooling water temperature;Температура охлаждающей жидкости;;°C;;;
;;6;Lube oil temperature;Температура масла;;°C;;;
;;7;Lube oil pressure at normal operation;Давление масла рабочее;;Мпа;;;
;;8;Lube oil pressure at min. steady speed;Давление масла при минимальных оборотах;;МПа;;;
;;9;Oil sump capacity;Объем масла;;литры;;;
;;10;Advance Injection Angle;Угол опережения впрыска;;°;;;
;;11;Min. idling steady speed;Холостые обороты;;об/мин;>;;
;;12;Steady regulation;Колебания оборотов;;%;;;
;;13;Valve timing inlet open;Угол открытия впускного клапана;;°;;;
;;14;Valve timing inlet close;Угол закрытия впускного клапана;;°;;;
;;15;Valve timing outlet open;Угол открытия выпускного клапана;;°;;;
;;16;Valve timing outlet close;Угол закрытия выпускного клапана;;°;;;
#;;Torque limits of main bolts and nuts/Моменты затяжки основных болтов и гаек;;;;;;;;;;;;;;
#TORQUE{;;;;;;;;;;;;;;;;
== Моменты затяжки основных болтов и гаек
#==EN Torque limits of main bolts and nuts
;;=#ID;=#Parameter;=Параметр;=#CH;=[MU];=eq;=Значение;=#Value
;;1;Cylinder head bolts;Болты головки блока;;Нм;;;
;;2;Main bearing cap bolts;Болты крышки коренных подшипников;;Нм;;;
;;3;Connecting rod bolts;Болты крышек шатунов;;Нм;;;
;;4;Flywheel bolts;Болты маховика;;Нм;;;
;;5;Crankshaft pulley blots;Болт шкива коленчатого вала;;Нм;;;
#;;Clearance of main parts /Установочные зазоры основных деталей;;;;;;;;;;;;;;
#CLEARANCE{;;;;;;;;;;;;;;;;
== Установочные зазоры основных деталей
#==EN Clearance of main parts 
;;=#ID;=#Parameter;=Параметр;=#CH;=[MU];=eq;=Значение;=#Value
;;1;Connecting rod journal and connecting rod bushing;Шатунная шейка коленчатого вала и втулка малой головки шатуна;;мм;;;
;;2;Piston pin and connecting;Поршневой палец;;мм;;;
;;3;Piston skirt and cylinder liner;Юбка поршня и гильза;;мм;;;
;;4;Side clearance between the 1st ring and its groove;Зазор между первым компрессионным кольцом и канавкой;;мм;;;
;;5;Side clearance between the 2nd ring its groove;Зазор между вторым компрессионным кольцом и канавкой;;мм;;;
;;6;Side clearance between the oil scraper ring and its groove;Зазор между маслосъемным кольцом и канавкой;;мм;;;
;;7;Gap of the 1st ring;Зазор замка первого компрессионного кольца;;мм;;;
;;8;Gap of the 2nd ring and oil scraper ring;Зазор замка второго компрессионного и маслосъемного колец;;мм;;;
;;9;Crankshaft main journal and main bearing;Шейка коленчатого вала;;мм;;;
;;10;Camshaft journal and bushing;Шейка распределительного вала;;мм;;;
;;11;Idle gear shaft journal and bushing;Шейка промежуточного вала;;мм;;;
;;12;Intake valve stem and valve guide;Направляющая впускного клапана;;мм;;;
;;13;Exhaust valve stem and valve guide;Направляющая выпускного клапана;;мм;;;
;;14;Rocker arm shaft and bushing;Шейка вала толкателей;;мм;;;
;;15;Axial clearance of crankshaft;Осевой зазор коленчатого вала;;мм;;;
;;16;Axial clearance of camshaft;Осевой зазор распределительного вала;;мм;;;
#;;Wear limit of main parts /Лимит износа основных деталей;;;;;;;;;;;;;;
#WEAR{;;;;;;;;;;;;;;;;
== Лимит износа основных деталей
#==EN Wear limit of main parts 
;;=#ID;=#Parameter;=Параметр;=#CH;=[MU];=eq;=Значение;=#Value
;;1;Connecting rod journal and connecting rod bushing;Шатунная шейка коленчатого вала и втулка малой головки шатуна;;мм;;;
;;2;Piston pin and connecting;Поршневой палец;;мм;;;
;;3;Piston skirt and cylinder liner;Юбка поршня и гильза;;мм;;;
;;4;Side clearance between the 1st ring and its groove;Зазор между первым компрессионным кольцом и канавкой;;мм;;;
;;5;Side clearance between the 2nd ring its groove;Зазор между вторым компрессионным кольцом и канавкой;;мм;;;
;;6;Side clearance between the oil scraper ring and its groove;Зазор между маслосъемным кольцом и канавкой;;мм;;;
;;7;Gap of the 1st ring;Зазор замка первого компрессионного кольца;;мм;;;
;;8;Gap of the 2nd ring and oil scraper ring;Зазор замка второго компрессионного и маслосъемного колец;;мм;;;
;;9;Crankshaft main journal and main bearing;Шейка коленчатого вала;;мм;;;
;;10;Camshaft journal and bushing;Шейка распределительного вала;;мм;;;
;;11;Idle gear shaft journal and bushing;Шейка промежуточного вала;;мм;;;
;;12;Intake valve stem and valve guide;Направляющая впускного клапана;;мм;;;
;;13;Exhaust valve stem and valve guide;Направляющая выпускного клапана;;мм;;;
;;14;Rocker arm shaft and bushing;Шейка вала толкателей;;мм;;;
;;15;Axial clearance of crankshaft;Осевой зазор коленчатого вала;;мм;;;
;;16;Axial clearance of camshaft;Осевой зазор распределительного вала;;мм;;;
