#
# Example of Makefile for daniad@
# deferred task: regular Makefile for this project
#

NODEFILE?="node/qrnode.js"
DOCSDIR?=docs/
NODE?=/usr/local/bin/node
CAT?=/bin/cat
TEST?=/bin/[

usage:
	@echo "Usage: make (run|noderun|nodecat)"
	@echo I do not know what to do!
all: build install run
run: noderun
clean: nodeclean
build:
	@echo nothing to build
install:
	@echo nothing to install yet
noderun:
	@echo "disabled by Eustrop due to incomplete & frozen state since 2020-05-04 (waiting for Node.js committer)"
	sh -c "exit 1"
	@[ -r ${NODEFILE} ]
	@${TEST} -d ${DOCSDIR} ]
	@echo "WARNING! node.js version of service not ready for production!"
	${NODE} node/qrnode.js
nodecat:
	@echo "disabled by Eustrop due to incomplete & frozen state since 2020-05-04 (waiting for Node.js committer)"
	sh -c "exit 1"
	 @${TEST} -r ${NODEFILE} ]
	${CAT} node/qrnode.js
nodeclean:
	@echo "disabled by Eustrop due to incomplete & frozen state since 2020-05-04 (waiting for Node.js committer)"
	sh -c "exit 1"
	rm -f node/toview.html

